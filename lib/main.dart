
import 'package:albumsapp/provider/Photos.dart';
import 'package:albumsapp/screens/homepage.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
          providers: [
            ChangeNotifierProvider(create:(context) => Photos()),
          ],
          child: MaterialApp(
          debugShowCheckedModeBanner: false,
          theme:ThemeData(
            primaryColor:Colors.deepPurple,
            accentColor: Colors.orange,
          ),
          home: HomePage(),
        
      ),
    );
  }
}