import 'package:albumsapp/provider/Photos.dart';
import 'package:albumsapp/screens/addnewImagescreen.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:provider/provider.dart';

class PhotosScreen extends StatefulWidget {
  final int albumId;
  PhotosScreen(this.albumId);

  @override
  _PhotosScreenState createState() => _PhotosScreenState();
}

class _PhotosScreenState extends State<PhotosScreen> {
  Future<List> photos;
  List<dynamic> photosCount;

  Future<List> getPhotosOfAlbum() async {
    var response = await Dio().get(
        "https://jsonplaceholder.typicode.com/albums/${widget.albumId}/photos");
    photosCount = await response.data;
    return response.data;
  }

  @override
  void initState() {
    photos = getPhotosOfAlbum();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _data = Provider.of<Photos>(context);

    return Scaffold(
        appBar: AppBar(
          title: Text("Photos"),
          actions: [
            IconButton(
                icon: Icon(Icons.add),
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (BuildContext context) {
                    return AddNewImage();
                  }));
                }),
          ],
        ),
        body: Column(
          children: [
            Expanded(
              child: FutureBuilder(
                  future: photos,
                  builder:
                      (BuildContext context, AsyncSnapshot<List> snapshot) {
                    if (snapshot.hasData) {
                      return Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: photosCount.length > 0
                              ? GridView.builder(
                                  gridDelegate:
                                      SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: 3),
                                  itemCount: photosCount.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Padding(
                                        padding: const EdgeInsets.all(5.0),
                                        child: GridTile(
                                          child: GestureDetector(
                                            child: Container(
                                              child: Center(
                                                  child: Image.network(
                                                photosCount[index]["url"],
                                                fit: BoxFit.cover,
                                              )),
                                            ),
                                            onTap: () {},
                                          ),
                                          footer: GridTileBar(
                                            backgroundColor: Colors.black12,
                                            trailing: IconButton(
                                              icon: Icon(
                                                Icons.delete,
                                                color: Colors.black26,
                                              ),
                                              onPressed: () {
                                                setState(() {
                                                  deletefromAPI(
                                                      photosCount[index]["id"]);
                                                  photosCount.remove(
                                                      photosCount[index]);
                                                });
                                              },
                                            ),
                                          ),
                                        ));
                                  })
                              : Center(
                                  child: Text("No Item"),
                                ));
                    }
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }),
            ),
            _data.listasset.length > 0
                ? Flexible(
                    child: GridView.count(
                      crossAxisCount: 3,
                      children: List.generate(_data.listasset.length, (index) {
                        Asset asset = _data.listasset[index];
                        return Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: AssetThumb(
                            asset: asset,
                            width: 20,
                            height: 20,
                          ),
                        );
                      }),
                    ),
                  )
                : InkWell(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        margin: EdgeInsets.all(2.0),
                        width: 100,
                        height: 50,
                        color: Colors.grey,
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.all(2.0),
                            child: FittedBox(
                              child: Text(
                                "new Images",
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
          ],
        ));
  }
}

Future deletefromAPI(int index) async {
  var response =
      await Dio().delete('https://jsonplaceholder.typicode.com/photos/$index');
  print("in Method:$response");
}
