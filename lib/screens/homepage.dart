import 'package:albumsapp/provider/Photos.dart';
import 'package:albumsapp/screens/albumsscreen.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';


class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Future<List> users;
  List usersCount;
  Future<List> getUsers() async {
    var response =
        await Dio().get("https://jsonplaceholder.typicode.com/users");
    usersCount = await response.data;
    return response.data;
  }

  @override
  void initState() {
    users = getUsers();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // final data = Provider.of<UsersData>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        title: Text("Choose a User"),
      ),
      body: FutureBuilder(
        future: users,
        builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
                itemCount: usersCount.length,
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                    margin: EdgeInsets.all(10),
                    elevation: 3.0,
                    child: ListTile(
                      leading: Text("${snapshot.data[index]["id"].toString()} -"),
                      title: Text(
                        snapshot.data[index]["name"],
                        style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),
                      ),
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (BuildContext context) =>
                                AlbumsScreen(snapshot.data[index]["id"]),
                          ),
                        );
                      },
                    ),
                  );
                });
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
