import 'package:albumsapp/screens/photosscreen.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class AlbumsScreen extends StatefulWidget {
  final int userId;
  AlbumsScreen(this.userId);

  @override
  _AlbumsScreenState createState() => _AlbumsScreenState();
}

class _AlbumsScreenState extends State<AlbumsScreen> {
  Future<List> albums;
  List albumsCount;

  Future<List> getAlbumsOfUser() async {
    var response = await Dio().get(
        "https://jsonplaceholder.typicode.com/users/${widget.userId}/albums");
    albumsCount = await response.data;
    return response.data;
  }

  @override
  void initState() {
    albums = getAlbumsOfUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Albums"),
      ),
      body: FutureBuilder(
          future: albums,
          builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
            if (snapshot.hasData) {
              return Padding(
                padding: const EdgeInsets.all(20.0),
                child: GridView.builder(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 1),
                    itemCount: albumsCount.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        margin: EdgeInsets.all(6),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(30),
                          child: GridTile(
                            child: GestureDetector(
                              child: Container(
                                padding: EdgeInsets.symmetric(horizontal: 50),
                                color: Colors.deepPurple,
                                child: Center(
                                  child: Text(
                                    snapshot.data[index]["title"],
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                              onTap: () {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        PhotosScreen(
                                      snapshot.data[index]["id"],
                                    ),
                                  ),
                                );
                              },
                            ),
                            footer: GridTileBar(
                              backgroundColor: Colors.black54,
                              title: Text(
                                "Album ${index+1}",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                      );
                    }),
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          }),
    );
  }
}
