import 'dart:convert';

import 'package:albumsapp/provider/Photos.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:provider/provider.dart';

class AddNewImage extends StatefulWidget {
  @override
  _AddNewImageState createState() => _AddNewImageState();
}

class _AddNewImageState extends State<AddNewImage> {
  List<Asset> images = List<Asset>();
  String _error = 'No Error Dectected';

  @override
  void initState() {
    super.initState();
  }

  Widget buildGridView() {
    return GridView.count(
      crossAxisCount: 3,
      children: List.generate(images.length, (index) {
        Asset asset = images[index];
        return Padding(
          padding: const EdgeInsets.all(5.0),
          child: AssetThumb(
            asset: asset,
            width: 300,
            height: 300,
          ),
        );
      }),
    );
  }

  Future<void> loadAssets() async {
    List<Asset> resultList = List<Asset>();
    String error = 'No Error Dectected';

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 20,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#abcdef",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
        ),
      );
    } on Exception catch (e) {
      error = e.toString();
    }
    if (!mounted) return;
    setState(() {
      images = resultList;

      _error = error;
    });
  }

  @override
  Widget build(BuildContext context) {
    final ph = Provider.of<Photos>(context, listen: false);
    Future<void> addNewImage() async{
      var body = jsonEncode(
        {
          "albumId": 0,
          "id": 0,
          "title": "test",
          "url": "test",
          "thumbnailUrl": "this is a test"
        },
      );
      var response = await Dio().post(
        "https://jsonplaceholder.typicode.com",
        data: body
      );
      print(response.data);
    }

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Add new Images"),
          backgroundColor: Colors.purple,
          actions: [
            FlatButton(
                onPressed: () {
                  ph.printlist();
                },
                child: Text("Print"))
          ],
        ),
        body: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(8.0),
              child: RaisedButton(
                child: Text("Pick images"),
                onPressed: loadAssets,
              ),
            ),
            Expanded(
              child: buildGridView(),
            ),
            FlatButton(
                color: Colors.purple.withOpacity(0.3),
                onPressed: () {
                  ph.setlist(images);
                  //Add new Images to API ...
                  //addNewImage();
                  Navigator.of(context).pop();
                },
                child: Text("Import")),
          ],
        ),
      ),
    );
  }
}

// import 'dart:html';

// import 'package:flutter/material.dart';
// import 'package:image_cropper/image_cropper.dart';
// import 'package:image_picker/image_picker.dart';

// class AddNewImageScreenState extends StatefulWidget {
//   @override
//   AddNewImageScreenStateState createState() => AddNewImageScreenStateState();
// }

// class AddNewImageScreenStateState extends State<AddNewImageScreenState> {
//   File _selectedFile;
//   bool _inProcess = false;

//   Widget getImageWidget() {
//     if (_selectedFile != null) {
//       return Image.file(
//         _selectedFile,
//         width: 250,
//         height: 250,
//         fit: BoxFit.cover,
//       );
//     } else {
//       return Image.asset(
//         "assets/placeholder.jpg",
//         width: 250,
//         height: 250,
//         fit: BoxFit.cover,
//       );
//     }
//   }

//   getImage(ImageSource source) async {
//       this.setState((){
//         _inProcess = true;
//       });
//       File image = await ImagePicker.pickImage(source: source);
//       if(image != null){
//         File cropped = await ImageCropper.cropImage(
//             sourcePath: image.path,
//             aspectRatio: CropAspectRatio(
//                 ratioX: 1, ratioY: 1),
//             compressQuality: 100,
//             maxWidth: 700,
//             maxHeight: 700,
//             compressFormat: ImageCompressFormat.jpg,
//             androidUiSettings: AndroidUiSettings(
//               toolbarColor: Colors.deepOrange,
//               toolbarTitle: "RPS Cropper",
//               statusBarColor: Colors.deepOrange.shade900,
//               backgroundColor: Colors.white,
//             )
//         );

//         this.setState((){
//           _selectedFile = cropped;
//           _inProcess = false;
//         });
//       } else {
//         this.setState((){
//           _inProcess = false;
//         });
//       }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Stack(
//         children: <Widget>[
//           Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               getImageWidget(),
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                 children: <Widget>[
//                   MaterialButton(
//                       color: Colors.green,
//                       child: Text(
//                         "Camera",
//                         style: TextStyle(color: Colors.white),
//                       ),
//                       onPressed: () {
//                         getImage(ImageSource.camera);
//                       }),
//                   MaterialButton(
//                       color: Colors.deepOrange,
//                       child: Text(
//                         "Device",
//                         style: TextStyle(color: Colors.white),
//                       ),
//                       onPressed: () {
//                         getImage(ImageSource.gallery);
//                       })
//                 ],
//               )
//             ],
//           ),
//           (_inProcess)?Container(
//             color: Colors.white,
//             height: MediaQuery.of(context).size.height * 0.95,
//             child: Center(
//               child: CircularProgressIndicator(),
//             ),
//           ):Center()
//         ],
//       )
//     );
//   }
// }
